# Lab 4 - Creating Custom Dashboards

At this point you have created a kubernetes cluster and were able to deploy your services onto it. Your app is running and  can start serving requests from customers. Everything looks fine, but wait! In order to bring this as a production-ready application, you must know the health of your services, you need some key performance indicators to understand their state.

In this lab you are going to build some dashboards using grafana and prometheus (which you may have guessed , come with istio for free). In order to do that, we are going to use some metrics called [" the four Golden Signals" ](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/#xref_monitoring_golden-signals)
These have become a industry  standard when designing monitoring systems for microservices and you should try to focus on them as an starting point.

The four metrics are:
- Latency
- Traffic
- Errors
- Saturation


## Part 1 - Accessing grafana
When you applied dashboard.yaml in lab 1, you exposed a set of telemetry tools using an istio ingressgateway. As you can see there, grafana is running on port 15031, so you can access it by opening in your web browser http://your.ingress.public-ip:15031

Get your ingress IP running:

`kubectl get svc -n istio-system | grep ingressgateway`


## Part 2 - Importing a dashboard to get started

In this lab, we will begin by importing the signals dashboard into grafana. Navigate to `http://your.ingress.public-ip:15031/dashboard/import`
and either paste the json from the 'Golden Signals Dashboard.json' file or upload it.

You should have a dashboard that looks something like this:

![alt text](nodeconf-signals.png "sample chart")  

Now, lets have a look at one of the measures by editing the Client Request Duration (Latency) chart. If you click the title and hit edit, you will see it queries prometheus with the following query:

`histogram_quantile(0.50, sum(irate(istio_request_duration_seconds_bucket{reporter="source",destination_service=~"$service"}[1m])) by (le))`

Lets unpack this a bit.

* `istio_request_duration_seconds_bucket` - This is the time series collected by istio. You can browse the time-series available in the prometheus dashboard in the graph section. http://your.ingress.public-ip/graph
* `{reporter="source",destination_service=~"$service"}` - These are filters on labels of the time-series. Note that a variable is used for the service name. Try switching the service in the drop-down provided in the dashboard.
* `[1m]` - This is a [range vector](https://prometheus.io/docs/prometheus/latest/querying/basics/#range-vector-selectors), which describes a lookback period for time-series information.
* [irate](https://prometheus.io/docs/prometheus/latest/querying/functions/#irate) - calculates the per-second instant rate of increase of the time series in the range vector. This is based on the last two data points.
* [histogram_quantile](https://prometheus.io/docs/prometheus/latest/querying/functions/#histogram_quantile) - Histograms and summaries both sample observations, typically request durations or response sizes. They track the number of observations and the sum of the observed values, allowing you to calculate the average of the observed values  

### Task 1 - Add some measures to calculate the 90th and 99th percentile

Calculating the median is fine, but it doesn't give us too much insight into our underlying service behaviour. Your task now will be to introduce new measures on the
first chart to track the 90th and 99th percentile of request latencies. When you are done, you should have a chart which looks like.

![alt text](nodeconf-latency.png "sample chart")  


### Task 2 - Correct the Saturation Filters to show information for the current service

Next lets take a look at our Saturation charts ( CPU & Memory usage ).

![alt text](nodeconf-saturation.png "sample chart")  

They seem a little underwhelming at the moment. It looks like it's showing us information from the wrong container! Correct the label filters so the charts show information from the currently selected service, as shown below.

![alt text](nodeconf-saturation-2.png "sample chart")  

Now we should be able to seamlessly switch between our services to visualize the performance of each service.


## Concluding Lab 4

You are now finished with Lab 4 in this lab we learned
* How to monitor critical operational signals of your microservices
* How to use time series, labels and functions to query Prometheus


In the next lab, we will look at [Fault Injection](../lab-5/README.md)
